package br.com.itau.marketplace.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.itau.marketplace.models.Produto;

@Service
public class ProdutoService {
	private List<Produto> produtos;
	private int idAtual;
	
	public ProdutoService() {
		produtos = new ArrayList<>();
		idAtual = 1;
		
		Produto produto1 = new Produto();
		produto1.setNome("Feijão");
		produto1.setPreco(10.00);
		
		inserirProduto(produto1);
	}
	
	public List<Produto> getProdutos() {
		return produtos;
	}
	
	public boolean inserirProduto(Produto produto) {
		produto.setId(idAtual);
		idAtual++;
		return produtos.add(produto);
	}
}
