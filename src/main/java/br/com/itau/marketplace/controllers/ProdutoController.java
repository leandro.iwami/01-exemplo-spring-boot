package br.com.itau.marketplace.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.marketplace.models.Produto;
import br.com.itau.marketplace.services.ProdutoService;

@RestController
@RequestMapping("/produto")
public class ProdutoController {
	@Autowired
	ProdutoService produtoService;

	@GetMapping
	public List<Produto> listarProdutos() {
		return produtoService.getProdutos();
	}
	
	@GetMapping("/{id}")
	public Produto listarProdutoPorId(@PathVariable int id) {
		List<Produto> produtos = produtoService.getProdutos();
		
		for(Produto produto : produtos) {
			if(produto.getId() == id) {
				return produto;
			}
		}
		
		return null;
	}
	
	@PostMapping
	public boolean inserirProduto(@RequestBody Produto produto) {
		return produtoService.inserirProduto(produto);
	}
}
